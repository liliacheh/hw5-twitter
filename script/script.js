import {getMultyData, addCards, insertLoading} from "./functions.js"

//Variables
const usersUrl = "https://ajax.test-danit.com/api/json/users";
const postsUrl = "https://ajax.test-danit.com/api/json/posts";
const root = document.querySelector('.root')

// animation
insertLoading(root)

//received users and posts info

getMultyData([usersUrl, postsUrl])
.then(res => addCards(res))
.then(cards => {
    document.querySelector('svg') ? document.querySelector('svg').remove() : undefined;
    cards.forEach(card => {
        root.appendChild(card.addCard());
    });
})
.catch(err => console.error(err));

export {root}


  