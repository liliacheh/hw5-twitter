import { addItem } from "./functions.js"
import{root} from "./script.js"
export class Card{
    constructor(id, userId, name, userName, email, title, body) {
        this.id = id
        this.userId = userId
        this.name = name
        this.userName = userName
        this.email = email
        this.title = title
        this.body = body
    }
    addCard(){
        const card = addItem('div', root, 'card', [{ 'data-id': this.getId()}]);
        card.insertAdjacentHTML('beforeend', `
        <div class="card__header">
            <h4 class="card__author">
                <span class="card__name">${this.getName()}</span>
                <span class="card__username">@${this.getUserName()}</span>
            </h4>
            <div class="card__btns">
                <button class="card__del-btn" type="button"></button>
            </div>
        </div>
        <p class="card__email">${this.getEmail()}</p>
        <p class="card__title">${this.getTitle()}</p>
        <p class="card__body">${this.getBody()}</p>
    `)
    const delBtn = card.querySelector('.card__del-btn');
    delBtn.addEventListener('click', () =>{
        this.deleteCard(this.getId())
    })
    return card
    }
    deleteCard(id) {
        fetch(`https://ajax.test-danit.com/api/json/posts/${id}`, {
            method: 'DELETE',
        })
            .then(res => res.ok ? document.querySelector(`[data-id='${id}']`).remove() : undefined)
            .catch(err => console.error(err))
    }
    getId() {
        return this.id
    }
    setId(num) {
        if (!isNaN(num)) this.id = num
    }
    getUserId() {
        return this.userId
    }
    setUserId(num) {
        if (!isNaN(num)) this.userId = num
    }
    getName() {
        return this.name
    }
    setName(str) {
        this.name = str
    }
    getUserName() {
        return this.userName
    }
    setUserName(str) {
        this.userName = str
    }
    getEmail() {
        return this.email
    }
    setEmail(str) {
        this.email = str
    }
    getTitle() {
        return this.title
    }
    setTitle(str) {
        this.title = str
    }
    getBody() {
        return this.body
    }
    setBody(str) {
        this.body = str
    }
}